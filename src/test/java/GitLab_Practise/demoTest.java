package GitLab_Practise;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class demoTest {

	public WebDriver driver;
	 String username="Admin";
	 String password="admin123";
	 String website="https://opensource-demo.orangehrmlive.com/index.php/auth/login";
	static String parentWindow;
	
	@BeforeClass
	public void chromeInitialization() {
	 //System.setProperty("webdriver.chrome.driver","exe/chromedriver.exe");
	 DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	 ChromeOptions ChromeOptions = new ChromeOptions();
    ChromeOptions.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
     //ChromeOptions.addArguments("incognito", "start-maximized", "window-size=1024,768", "--no-sandbox");
    // System.setProperty("webdriver.chrome.driver","https://gitlab.com/Sowmya.amy21/demoproject/tree/master/exe/chromedriver.exe");
     //ChromeOptions.addArguments("start-maximized");
     capabilities.setCapability(ChromeOptions.CAPABILITY, ChromeOptions);
     driver = new ChromeDriver(ChromeOptions);
	}
	@Test(priority=2)
	public void openWebsite()
	{
	    System.out.println("-----------Open Website------------");
	  driver.get(website);
	  parentWindow=driver.getWindowHandle();
	}
	@Test(priority=5)
	public void maximizeWindow()
	{
	    System.out.println("------------Maximize window------------");
	  driver.manage().window().maximize();
	}
	
	@Test(priority=3)
	public void login()
	{
	    System.out.println("------------login------------");
		driver.switchTo().window(parentWindow);
		driver.findElement(By.id("txtUsername")).sendKeys(username);
		driver.findElement(By.id("txtPassword")).sendKeys(password);
		//driver.findElement(By.id("btnLogin")).click();
		
	}
	@Test(priority=4)
	public void login1()
	{
		System.out.println("------------click on login button------------");
		driver.findElement(By.xpath("btnLogin")).click();
		
	}
	
	@AfterClass
	public void quitBrowser()
	{
	    System.out.println("------------After Class------------");
		driver.quit();
	}
}
